import os
import subprocess
import argparse
import torch

from fid_pytorch.fid_func import *
from fid_pytorch.datasets import *
from fid_pytorch.inception import InceptionV3

def split_frame(vidname, img_name: str = '%05d.jpg'):
    dir_output, _ = os.path.split(vidname)
    dir_output = os.path.join(dir_output, 'frames')
    os.makedirs(dir_output, exist_ok=True)

    ffmpeg_commands = [
        'ffmpeg', '-y',
        '-hide_banner',
        '-loglevel', 'error',
        '-i', vidname,
        f'{dir_output}/{img_name}'
    ]

    ffmpeg_command = ' '.join(ffmpeg_commands)
    subprocess.call(ffmpeg_command, shell=True)

    return dir_output


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--batch-size', type=int, default=50,
                    help='Batch size to use')
    
    parser.add_argument('--num_workers', type=int,
                    help=('Number of processes to use for data loading. '
                          'Defaults to `min(8, num_cpus)`'))

    parser.add_argument('--device', type=str, default=None,
                    help='Device to use. Like cuda, cuda:0 or cpu')
    
    parser.add_argument('--dims', type=int, default=2048,
                    choices=list(InceptionV3.BLOCK_INDEX_BY_DIM),
                    help=('Dimensionality of Inception features to use. '
                          'By default, uses pool3 features'))
    
    parser.add_argument('-g, --generated', dest='generated', type=str, required=True,
                    help=('Paths to the generated vid'))
    
    parser.add_argument('-tr, --target_root', dest='target_root', type=str, required=True,
                    help=('Paths to the Ground Truth vid'))

    parser.add_argument('-tn1, --target_name1', dest='target_name1', type=str, required=True,
                    help=('Paths to the Ground Truth vid'))
    
    parser.add_argument('-tn2, --target_name2', dest='target_name2', type=str, required=True,
                    help=('Paths to the Ground Truth vid'))
    
    return parser.parse_args()


def main():
    # Setup
    args = get_args()

    if args.device is None:
        device = torch.device('cuda' if (torch.cuda.is_available()) else 'cpu')
    else:
        device = torch.device(args.device)

    if args.num_workers is None:
        num_avail_cpus = len(os.sched_getaffinity(0))
        num_workers = min(num_avail_cpus, 8)
    else:
        num_workers = args.num_workers

    # Prepare Data

    dir_gen = split_frame(args.generated)
    
    target_list1 = sorted([os.path.join(args.target_root, 'image', args.target_name1, x) 
                        for x in os.listdir(os.path.join(args.target_root, 'image', args.target_name1)) if x.split('.')[-1].lower() in ['jpg', 'png', 'jpeg']])

    target_list2 = sorted([os.path.join(args.target_root, 'image', args.target_name2, x) 
                        for x in os.listdir(os.path.join(args.target_root, 'image', args.target_name2)) if x.split('.')[-1].lower() in ['jpg', 'png', 'jpeg']])
    
    pkl_list1 = sorted([os.path.join(args.target_root, 'landmark', args.target_name1, x) 
                        for x in os.listdir(os.path.join(args.target_root, 'landmark', args.target_name1)) if x.split('.')[-1] == 'pkl'])
    
    pkl_list2 = sorted([os.path.join(args.target_root, 'landmark', args.target_name2, x) 
                        for x in os.listdir(os.path.join(args.target_root, 'landmark', args.target_name2)) if x.split('.')[-1] == 'pkl'])

    dataset_gen = BasicDataset(dir_gen, pkl_list1)
    dataset_trg1 = BasicDataset(target_list1, pkl_list1)
    dataset_trg2 = BasicDataset(target_list2, pkl_list2)

    # Calculate Statistics

    mg, sg = calculate_statistics_each(dataset_gen, args.batch_size, device, args.dims, num_workers=num_workers)

    mt1, st1 = calculate_statistics_each(dataset_trg1, args.batch_size, device, args.dims, num_workers=num_workers)

    mt2, st2 = calculate_statistics_each(dataset_trg2, args.batch_size, device, args.dims, num_workers=num_workers)

    # Get FID Score

    fid_AB = calculate_frechet_distance(mt1, st1, mt2, st2)

    fid_GA = calculate_frechet_distance(mg, sg, mt1, st1)

    fid_GB = calculate_frechet_distance(mg, sg, mt2, st2)

    # Log

    print(f'FID score Between GT A, B: {fid_AB}')
    print(f'FID score Between Generated A and GT A: {fid_GA}')
    print(f'FID score Between Generated A and GT B: {fid_GB}')


if __name__ == '__main__':
    main()