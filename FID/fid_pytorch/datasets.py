import os
import numpy as np
from PIL import Image
from torchvision import transforms
from torch.utils.data import Dataset
import pickle
import torch
import cv2

import face_alignment.api as fa

class BasicDataset(Dataset):
    def __init__(self, dir_files, dir_pickle, bbox_padding=0.0):
        if isinstance(dir_files, str):
            self.files = sorted([os.path.join(dir_files, x) for x in os.listdir(dir_files) if x.split('.')[-1] == 'jpg'])
        elif isinstance(dir_files, list):
            self.files = dir_files
        else:
            raise ValueError(type(dir_files))
        with open(dir_pickle[0], 'rb') as f:
            data = pickle.load(f)

        self.bbox_first = data['bbox']
        
        self.landmarks = dir_pickle
        self.bbox_padding=bbox_padding
        self.transforms = transforms.ToTensor()

    def __len__(self):
        return len(self.files)

    def __getitem__(self, i):
        path = self.files[i]
        img = Image.open(path).convert('RGB')

        if self.bbox_first is not None:
            x1, y1, x2, y2 = self.bbox_first
            x_ = (x2 - x1)
            y_ = (y2 - y1)
            X1 = int(max(0, x1 - x_ * self.bbox_padding))
            Y1 = int(max(0, y1 - y_ * self.bbox_padding))
            X2 = int(min(x2 + x_ * self.bbox_padding, img.width))
            Y2 = int(min(y2 + y_ * self.bbox_padding, img.height))

            bbox = (X1, Y1, X2, Y2)

        else:
            with open(self.landmarks[i], 'rb') as f:
                data = pickle.load(f)
            bbox = tuple(data['bbox'])

        img = img.crop(bbox)

        img = img.resize((512,512))

        img = self.transforms(img)
        return img

class HalfDataset(BasicDataset):
    def __getitem__(self, i):
        path = self.files[i]
        img = Image.open(path).convert('RGB')

        if self.bbox_first is not None:
            x1, y1, x2, y2 = self.bbox_first
            x_ = (x2 - x1)
            y_ = (y2 - y1)
            X1 = int(max(0, x1 - x_ * self.bbox_padding))
            Y1 = int(max(0, y1 - y_ * self.bbox_padding))
            X2 = int(min(x2 + x_ * self.bbox_padding, img.width))
            Y2 = int(min(y2 + y_ * self.bbox_padding, img.height))

            bbox = (X1, Y1//2, X2, Y2)

        else:
            with open(self.landmarks[i], 'rb') as f:
                data = pickle.load(f)
            X1, Y1, X2, Y2 = data['bbox']
            bbox = (X1, Y1//2, X2, Y2)

        img = img.crop(bbox)
        img = img.resize((512,512))

        img = self.transforms(img)
        return img

class FIDDataset(Dataset):
    def __init__(self, dir_files, dir_pickle=None, use_half=False, bbox_padding=0.0):
        
        self.image_list = [os.path.join(dir_files, x) for x in sorted(os.listdir(dir_files)) if x.split('.')[-1].lower() in ['jpg', 'png', 'jpeg']]

        if dir_pickle is not None:
            self.do_crop = True
            self.pickle_list = sorted([os.path.join(dir_pickle, x) for x in os.listdir(dir_pickle) if x.split('.')[-1].lower() == 'pkl'])
            self.bbox_padding=bbox_padding

            with open(self.pickle_list[0], 'rb') as f:
                self.bbox_first_frame = pickle.load(f)['bbox']
        else:
            self.do_crop = False
            
        self.use_half = use_half

    def __len__(self):
        return len(self.image_list)

    def __getitem__(self, i):
        img = cv2.imread(self.image_list[i])
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        if self.do_crop:
            if self.bbox_first_frame is None:
                with open(self.pickle_list[i], 'rb') as f:
                    x1, y1, x2, y2 = pickle.load(f)['bbox']
            else:
                x1, y1, x2, y2 = self.bbox_first_frame

            x_ = (x2 - x1)
            y_ = (y2 - y1)
            X1 = int(max(0, x1 - x_ * self.bbox_padding))
            Y1 = int(max(0, y1 - y_ * self.bbox_padding))
            X2 = int(min(x2 + x_ * self.bbox_padding, img.shape[1]))
            Y2 = int(min(y2 + y_ * self.bbox_padding, img.shape[0]))

            img = img[Y1:Y2, X1:X2]

        img = cv2.resize(img, dsize=(512,512))

        img = img / 255.
        img = img.transpose(2, 0, 1)  # (C * T) x H x W
        
        if self.use_half:
            img = img[:, img.shape[1]//2:]

        return torch.FloatTensor(img)