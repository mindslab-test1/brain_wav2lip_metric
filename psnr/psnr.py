import os
import subprocess
import argparse
import torch
import pickle

from PIL import Image
from torchvision import transforms

def split_frame(vidname, img_name: str = '%05d.jpg'):
    dir_output, _ = os.path.split(vidname)
    dir_output = os.path.join(dir_output, 'frames')
    os.makedirs(dir_output, exist_ok=True)

    ffmpeg_commands = [
        'ffmpeg', '-y',
        '-hide_banner',
        '-loglevel', 'error',
        '-i', vidname,
        f'{dir_output}/{img_name}'
    ]

    ffmpeg_command = ' '.join(ffmpeg_commands)
    subprocess.call(ffmpeg_command, shell=True)

    return dir_output

def crop_face(img, pkl, bbox_first=None, half=False, bbox_padding=0.0):
    if bbox_first is not None:
        x1, y1, x2, y2 = bbox_first
        x_ = (x2 - x1)
        y_ = (y2 - y1)
        X1 = int(max(0, x1 - x_ * bbox_padding))
        Y1 = int(max(0, y1 - y_ * bbox_padding))
        X2 = int(min(x2 + x_ * bbox_padding, img.width))
        Y2 = int(min(y2 + y_ * bbox_padding, img.height))

    else:
        with open(pkl, 'rb') as f:
            data = pickle.load(f)
        
        X1, Y1, X2, Y2 = data['bbox']
    
    if half:
        bbox = (X1, Y1//2, X2, Y2)
        return img.crop(bbox).resize((512, 256))
    else:
        bbox = (X1, Y1, X2, Y2)
        return img.crop(bbox).resize((512, 512))

def cycle(iterator):
    while True:
        for batch in iterator:
            yield batch

def calculate_psnr(generated, target):
    
    mse = torch.mean((generated - target) ** 2)
    
    return 20 * torch.log10(255.0 / torch.sqrt(mse))

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-g, --generated', dest='generated', type=str, required=True,
                    help=('Paths to the generated vid'))
    
    parser.add_argument('-tr, --target_root', dest='target_root', type=str, required=True,
                    help=('Paths to the Ground Truth vid'))

    parser.add_argument('-tn, --target_name', dest='target_name', type=str, required=True,
                    help=('Paths to the Ground Truth vid'))
    
    return parser.parse_args()

def main():
    args = get_args()

    dir_gen = split_frame(args.generated)
    gen_list = sorted([os.path.join(dir_gen, x) for x in os.listdir(dir_gen)])
    
    target_list = sorted([os.path.join(args.target_root, 'image', args.target_name, x) 
                        for x in os.listdir(os.path.join(args.target_root, 'image', args.target_name)) if x.split('.')[-1].lower() in ['jpg', 'png', 'jpeg']])
    
    pkl_list = sorted([os.path.join(args.target_root, 'landmark', args.target_name, x)
                        for x in os.listdir(os.path.join(args.target_root, 'landmark', args.target_name)) if x.split('.')[-1] == 'pkl'])


    if len(gen_list) > len(target_list):
        gen_list = gen_list[:len(target_list)]

    fnc = transforms.ToTensor()
    
    scores = []

    with open(pkl_list[0], 'rb') as f:
        data = pickle.load(f)
    bbox_first = data['bbox']

    for generated, target, pkl in zip(gen_list, target_list, pkl_list):
        generated = Image.open(generated)
        target = Image.open(target)

        generated = fnc(crop_face(generated, pkl, bbox_first=bbox_first, half=False))
        target = fnc(crop_face(target, pkl, bbox_first=bbox_first, half=False))

        scores.append(calculate_psnr(generated, target).item())
    
    print(f'Average: {sum(scores) / len(scores)}\tMax: {max(scores)}\tMin: {min(scores)}')

if __name__=='__main__':
    main()


