import os
import argparse
from pathlib import Path
import logging
import torch
from torch.utils.data import DataLoader
from tqdm import tqdm
import numpy as np

from syncnet import MetricDataset, SyncNet, load_checkpoint, eval_model
from FID import FIDDataset, InceptionV3, calculate_statistics_each, calculate_frechet_distance
from psnr import calculate_psnr, cycle

from time import time

def calculate_syncnet_metric(generated_frame_path, generated_audio_path,
                                     original_frame_path, original_audio_path, 
                                     model, device, batch_size, num_workers, syncnet_thre=0.5):

    dataset_gen = MetricDataset(audio_path=generated_audio_path,
                    image_dir=generated_frame_path, device=device)

    dataset_trg = MetricDataset(audio_path=original_audio_path,
                        image_dir=original_frame_path, device=device)

    dataloader_gen = DataLoader(dataset_gen, batch_size=batch_size, shuffle=False,
                            num_workers=num_workers)

    dataloader_trg = DataLoader(dataset_trg, batch_size=batch_size, shuffle=False,
                            num_workers=num_workers)

    out_gen = eval_model(dataloader_gen, device, model.eval(), accuracy_threshold=syncnet_thre)
    out_trg = eval_model(dataloader_trg, device, model.eval(), accuracy_threshold=syncnet_thre)

    out_gen['dataset_len'] = len(dataset_gen)
    out_trg['dataset_len'] = len(dataset_trg)

    return out_gen, out_trg

def calculate_fid_metric(generated_frame_path, generated_pkl_path, original_frame_path, original_pkl_path, 
                            device, batch_size, num_workers, dims, use_half):
    
    out = {}
    # Prepare Data

    dataset_gen = FIDDataset(generated_frame_path, dir_pickle=generated_pkl_path, use_half=use_half)
    
    dataset_trg = FIDDataset(original_frame_path, dir_pickle=original_pkl_path, use_half=use_half)

    # Calculate Statistics

    mean_gen, std_gen = calculate_statistics_each(dataset_gen, batch_size, device, dims, num_workers=num_workers)

    mean_trg, std_trg = calculate_statistics_each(dataset_trg, batch_size, device, dims, num_workers=num_workers)

    # Get FID Score

    fid_AB = calculate_frechet_distance(mean_gen, std_gen, mean_trg, std_trg)
    out['FID'] = fid_AB
    out['dataset_len'] = [len(dataset_gen), len(dataset_trg)]

    # Match Length
    if len(dataset_gen) > len(dataset_trg):
        pbar = tqdm(range(len(dataset_trg)), disable=logging.getLogger().getEffectiveLevel()>10)
    else:
        pbar = tqdm(range(len(dataset_gen)), disable=logging.getLogger().getEffectiveLevel()>10)
    
    psnr_scores = []

    # Calculate PSNR Score
    iterator_gen = cycle(dataset_gen)
    iterator_trg = cycle(dataset_trg)

    for _ in pbar:
        data_gen = next(iterator_gen)
        data_trg = next(iterator_trg)
        psnr_scores.append(calculate_psnr(data_gen, data_trg).item())
    
    out['psnr_score'] = psnr_scores

    return out

def check_dir(dir):
    bools = [os.path.isdir(os.path.join(dir, x)) for x in os.listdir(dir)]
    assert all(bools[0] == e for e in bools)
    return bools[0]

def run_metric(args, device):
    time_sync = []
    time_fid = []
    if not args.is_folder:
        if args.use_syncnet:
            
            checkpoint_path = Path(args.checkpoint_path)
            model = SyncNet().to(device)
            print('total trainable params {}'.format(sum(p.numel() for p in model.parameters() if p.requires_grad)))

            if checkpoint_path is not None:
                model = load_checkpoint(checkpoint_path, model)
            
            out_gen, out_trg = calculate_syncnet_metric(device=device, model=model, **vars(args))
            print("##### RESULT WITH PRETRAINED SYNCNET #####")
            print(f"Generated : {out_gen['dataset_len']} frames")
            print(f"Original : {out_trg['dataset_len']} frames")
            print(f"\tGenerated|average_bce_loss: {out_gen['average_bce_loss']:.4f}")
            print(f"\tGenerated|average_cossim: {out_gen['average_cossim']:.4f}")
            print(f"\tGenerated|average_accuracy: {out_gen['average_accuracy'] * 100:.2f} % (with threshold {out_gen['accuracy_threshold']:.2f})")

            print(f"\tOriginal|average_bce_loss: {out_trg['average_bce_loss']:.4f}")
            print(f"\tOriginal|average_cossim: {out_trg['average_cossim']:.4f}")
            print(f"\tOriginal|average_accuracy: {out_trg['average_accuracy'] * 100:.2f} % (with threshold {out_trg['accuracy_threshold']:.2f})")
        
        if args.use_fid_psnr:
            out = calculate_fid_metric(device=device, **vars(args))
            print("##### RESULT WITH FID #####")
            print(f"Generated : {out['dataset_len'][0]} frames")
            print(f"Original : {out['dataset_len'][1]} frames")
            print(f"\tFID Score: {out['FID']:.4f}")
            print(f"\taverage_PSNR: {(sum(out['psnr_score']) / len(out['psnr_score'])):.4f}")
            print(f"\tmax_PSNR per batch: {max(out['psnr_score']):.4f}")
            print(f"\tmin_PSNR per batch: {min(out['psnr_score']):.4f}")
    
    else:
        vidnames = [x for x in os.listdir(args.generated_frame_path)]
        out_gen, out_trg, out = {}, {}, {}

        if args.use_syncnet:
            checkpoint_path = Path(args.checkpoint_path)
            model = SyncNet().to(device)
            print('total trainable params {}'.format(sum(p.numel() for p in model.parameters() if p.requires_grad)))

            if checkpoint_path is not None:
                model = load_checkpoint(checkpoint_path, model)
        start_all = time()
        for ii, vidname in tqdm(enumerate(vidnames), total=len(vidnames), disable=logging.getLogger().getEffectiveLevel()>20):
            if args.use_syncnet:
                sync_start = time()
                cur_generated_frame_path = os.path.join(args.generated_frame_path, vidname)
                cur_generated_audio_path = os.path.join(args.generated_audio_path, vidname + '.wav')

                cur_original_frame_path = os.path.join(args.original_frame_path, vidname)
                cur_original_audio_path = os.path.join(args.original_audio_path, vidname + '.wav')

                cur_out_gen, cur_out_trg = calculate_syncnet_metric(cur_generated_frame_path, cur_generated_audio_path,
                                     cur_original_frame_path, cur_original_audio_path, model, device, args.batch_size, args.num_workers, args.syncnet_thre)
                if ii == 0:
                    out_gen = cur_out_gen
                    out_trg = cur_out_trg
                else:
                    for key in out_gen.keys():
                        if key != 'accuracy_threshold':
                            out_gen[key] += cur_out_gen[key]
                            out_trg[key] += cur_out_trg[key]
                time_sync.append(time() - sync_start)
            
            if args.use_fid_psnr:
                fid_start = time()
                cur_generated_frame_path = os.path.join(args.generated_frame_path, vidname)
                cur_generated_pkl_path = os.path.join(args.generated_pkl_path, vidname) if args.generated_pkl_path is not None else None

                cur_original_frame_path = os.path.join(args.original_frame_path, vidname)
                cur_original_pkl_path = os.path.join(args.original_pkl_path, vidname) if args.original_pkl_path is not None else None

                cur_out = calculate_fid_metric(cur_generated_frame_path, cur_generated_pkl_path, cur_original_frame_path, cur_original_pkl_path,
                                                     device, args.batch_size, args.num_workers, args.dims, args.use_half)

                if ii == 0:
                    out = cur_out
                else:
                    for key in out.keys():
                        if key == 'psnr_score':
                            out[key].extend(cur_out[key])
                        elif key == 'dataset_len':
                            out[key][0] += cur_out[key][0]
                            out[key][1] += cur_out[key][1]
                time_fid.append(time() - fid_start)
        total_time = time() - start_all
        print(f"Number of videos : {len(vidnames)}")    
        
        if args.use_syncnet:
            for key in out_gen.keys():
                if key not in ['accuracy_threshold', 'dataset_len']:
                    out_gen[key] /= len(vidnames)
                    out_trg[key] /= len(vidnames)

            print("##### RESULT WITH PRETRAINED SYNCNET #####")
            print(f"Generated : {out_gen['dataset_len']} frames")
            print(f"Original : {out_trg['dataset_len']} frames")
            print(f"\tGenerated | average_bce_loss: {out_gen['average_bce_loss']:.4f}")
            print(f"\tGenerated | average_cossim: {out_gen['average_cossim']:.4f}")
            print(f"\tGenerated | average_accuracy: {out_gen['average_accuracy'] * 100:.2f} % (with threshold {out_gen['accuracy_threshold']:.2f})")

            print(f"\tOriginal | average_bce_loss: {out_trg['average_bce_loss']:.4f}")
            print(f"\tOriginal | average_cossim: {out_trg['average_cossim']:.4f}")
            print(f"\tOriginal | average_accuracy: {out_trg['average_accuracy'] * 100:.2f} % (with threshold {out_trg['accuracy_threshold']:.2f})")
        
        if args.use_fid_psnr:
            out['FID'] /= len(vidnames)

            print("##### RESULT WITH FID #####")
            print(f"Generated : {out['dataset_len'][0]} frames")
            print(f"Original : {out['dataset_len'][1]} frames")
            print(f"\tFID Score : {out['FID']:.4f}")
            print(f"\taverage_PSNR : {(sum(out['psnr_score']) / len(out['psnr_score'])):.4f}")
            print(f"\tmax_PSNR per batch : {max(out['psnr_score']):.4f}")
            print(f"\tmin_PSNR per batch : {min(out['psnr_score']):.4f}")
        time_sync = np.array(time_sync)
        time_fid = np.array(time_fid)
        print(f'Total time : {total_time:.4f} sec')
        print(f'Average time consumed for syncnet calculation : {time_sync.mean():.4f} sec')
        print(f'Average time consumed for FID&PSNR calculation : {time_fid.mean():.4f} sec')
    

def parse_args():
    
    parser = argparse.ArgumentParser(description='Code to evaluate Lipsync model')

    parser.add_argument("--generated_frame_path", type=str, help="Generated frames path", default=None)
    parser.add_argument("--generated_audio_path", type=str, help="Audio path for generated output", default=None)
    parser.add_argument("--generated_pkl_path", type=str, help="Pickle File path for generated output", default=None)

    parser.add_argument("--original_frame_path", type=str, help="Original frames path", default=None)
    parser.add_argument("--original_audio_path", type=str, help="Audio path for original video", default=None)
    parser.add_argument("--original_pkl_path", type=str, help="Pickle File path for original video", default=None)

    parser.add_argument("--batch_size", type=int, help="batch size", default=1)
    parser.add_argument("--num_workers", type=int, help="num_workers", default=1)
    parser.add_argument("--device", type=str, help="device", default='cuda')
    parser.add_argument('--checkpoint_path', help='Resumed from this checkpoint', default="/DATA/lipsync/lipsync_expert.pth", type=str)
    parser.add_argument('--dims', type=int, default=2048, choices=list(InceptionV3.BLOCK_INDEX_BY_DIM), 
                            help='Dimensionality of Inception features to use. By default, uses pool3 features')

    parser.add_argument("--use_syncnet", help="Choose to use syncnet or not", action='store_true')
    parser.add_argument("--syncnet_thre", help='Choose threshold value for syncnet accuracy. Default=0.5', type=float, default=0.5)
    parser.add_argument("--use_fid_psnr", help="Choose to use FID&PSNR or not", action='store_true')
    parser.add_argument("--use_half", help="Use half bottom of face for FID", action='store_true')

    parser.add_argument("--log_level", type=str, default='INFO')

    args = parser.parse_args()
    return args




if __name__ == '__main__':
    args = parse_args()
    device = torch.device(args.device)

    logging.getLogger().setLevel(getattr(logging, args.log_level))

    assert args.generated_audio_path is not None
    assert args.generated_frame_path is not None
    is_folder_g = check_dir(args.generated_frame_path)
    print(f"Generated (frame) : {os.path.abspath(args.generated_frame_path)}\nAudio for Generated : {os.path.abspath(args.generated_audio_path)}")
    if args.generated_pkl_path is not None:
        print(f"Pickles for Generated : {os.path.abspath(args.generated_pkl_path)}")

    
    assert args.original_audio_path is not None
    assert args.original_frame_path is not None
    is_folder_o = check_dir(args.original_frame_path)
    print(f"Original (frame) : {os.path.abspath(args.original_frame_path)}\nAudio for Original : {os.path.abspath(args.original_audio_path)}")
    if args.original_pkl_path is not None:
        print(f"Pickles for Original : {os.path.abspath(args.original_pkl_path)}")
    
    assert is_folder_g == is_folder_o
    args.is_folder = is_folder_g
    print("\nFolders") if args.is_folder else print("\nFrames")
    
    run_metric(args, device)