"""
This code is obtained from
https://raw.githubusercontent.com/Rudrabha/Wav2Lip/master/color_syncnet_train.py
"""
import os

from .syncnet import SyncNet

import torch.nn as nn
import torch
import torch.nn.functional as F
from torch.utils import data as data_utils

import numpy as np
import librosa

import cv2
import argparse
from pathlib import Path

import face_alignment.api as fa

import subprocess
import logging
from .audio import load_wav, melspectrogram


FPS = 25
SAMPLING_RATE = 16000
FILE_STARTING_INDEX = 1

syncnet_T = 5
syncnet_mel_step_size = 16

class MetricDataset(object):
    def __init__(self, audio_path, image_dir, device=None):
        self.audio_path = audio_path
        self.image_dir = image_dir
        
        self.fa = fa.FaceAlignment(fa.LandmarksType._2D)
        
        image_list = self.get_image_list(self.image_dir)
        mel = self.get_audio(self.audio_path)
        
        self.cropped_image_list = self.get_cropped_image(image_list)
        self.mel = mel
        # print(self.mel.shape)
        
    def get_cropped_image(self, image_list, imsize=96):
        _image_list = []
        for idx, image in enumerate(image_list):
            x1, y1, x2, y2 = self.fa.get_detection(image)
            _image = image[y1:y2, x1:x2]
            _image_list.append(cv2.resize(_image, (imsize, imsize)))
                
        return _image_list
    
    @staticmethod
    def get_image_list(image_dir):
        image_paths = sorted([str(child) for child in Path(image_dir).glob("*.png")])
        image_list = [cv2.imread(str(image_path)) for image_path in image_paths]
        return image_list
        

    @staticmethod
    def get_audio(audio_path):
        audio = load_wav(audio_path, sr=SAMPLING_RATE)  # 16kHz load
        mel = melspectrogram(audio).T
        return mel

    def get_window(self, idx):
        return self.cropped_image_list[idx:idx+syncnet_T]

    @staticmethod
    def crop_audio_window(spec, idx):
        # num_frames = (T x hop_size * fps) / sample_rate
        start_idx = int(float(syncnet_mel_step_size * 5) * (idx / float(FPS)))
        end_idx = start_idx + syncnet_mel_step_size
        return spec[start_idx:end_idx, :]


    def __len__(self):
        return min(len(self.cropped_image_list) - syncnet_T,
                   int((len(self.mel) - syncnet_mel_step_size) / (syncnet_mel_step_size * 5) * FPS))

    def __getitem__(self, idx):
        mel = self.crop_audio_window(self.mel, idx).copy()

        if len(self.cropped_image_list) <= 3 * syncnet_T:
            raise AssertionError(f"The number of frame should be larger than {syncnet_T * 3}. (Now: ({len(self.iamge_list)}))")
        
        window = self.get_window(idx)

        # H x W x 3 * T
        x = np.concatenate(window, axis=2) / 255.
        x = x.transpose(2, 0, 1)  # (C * T) x H x W
        x = x[:, x.shape[1]//2:]

        x = torch.FloatTensor(x)
        mel = torch.FloatTensor(mel.T).unsqueeze(0)

        return x, mel


def cosine_loss(audio, video, ones_tensor):
    sim = F.cosine_similarity(audio, video)
    loss = F.binary_cross_entropy(sim, ones_tensor)
    return sim, loss


@torch.no_grad()
def eval_model(dataloader, device, model, accuracy_threshold=0.5):
    losses = []
    sims = np.empty([0])
    for step, (x, mel) in enumerate(dataloader):
        x = x.to(device)
        mel = mel.to(device)

        audio, video = model(mel, x)
        sim, loss = cosine_loss(audio, video, torch.ones(x.size(0)).float().to(device))
        losses.append(loss.item())
        sims = np.append(sims, sim.cpu().numpy(), axis=0)

    averaged_loss = sum(losses) / len(losses)
    averaged_sim = sum(sims) / len(sims)
    average_accuracy = sum(sims >= accuracy_threshold) / sims.size

    return {"average_bce_loss": averaged_loss,
            "average_cossim": averaged_sim,
            "average_accuracy": average_accuracy,
            "accuracy_threshold": accuracy_threshold}


def load_checkpoint(checkpoint_path, model):

    print(f"Load checkpoint from: {checkpoint_path}")

    checkpoint = torch.load(checkpoint_path, map_location=lambda storage, loc: storage)
    model.load_state_dict(checkpoint["state_dict"])
    
    return model