from .metric import MetricDataset, SyncNet, load_checkpoint, eval_model
import argparse

import torch
from pathlib import Path
from torch.utils.data import DataLoader

def parse_args():
    
    parser = argparse.ArgumentParser(description='Code to train the expert lip-sync discriminator')

    parser.add_argument("--audio_path", type=str, help="audio path", default=None)
    parser.add_argument("--image_dir", type=str, help="image dir", default=None)
    parser.add_argument("--video_path", type=str, help="video path", default=None)
    parser.add_argument("--batch_size", type=int, help="batch size", default=1)
    parser.add_argument("--num_workers", type=int, help="num_workers", default=1)
    parser.add_argument('--checkpoint_path', help='Resumed from this checkpoint', default="/DATA/lipsync/lipsync_expert.pth", type=str)

    args = parser.parse_args()
    return args

def pretty_print(audio_path, image_dir, video_path, use_frame, use_video,
                 average_bce_loss=None, average_cossim=None,
                 average_accuracy=None, accuracy_threshold=None,
                 **kwargs):
    print("##### RESULT WITH PRETRAINED SYNCNET #####")
    if use_frame:
        print(f"use_frame | audio: {audio_path} / frame: {image_dir}")
    elif use_video:
        print(f"use_video | video: {video_path}")
    else:
        raise AssertionError("You should give either (audio, image) or (video).")
    print(f"average_bce_loss: {average_bce_loss:.4f}")
    print(f"average_cossim: {average_cossim:.4f}")
    print(f"average_accuracy: {average_accuracy * 100:.2f} % (with threshold {accuracy_threshold:.2f})")

    
if __name__ == '__main__':
    args = parse_args()
    checkpoint_path = Path(args.checkpoint_path)

    use_frame = args.audio_path is not None and args.image_dir is not None
    use_video = args.video_path is not None
    
    # TODO: designate gpu
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    if use_frame:
        dataset = MetricDataset(audio_path=args.audio_path,
                          image_dir=args.image_dir, use_frame=use_frame, device=device)
        
    elif use_video:
        dataset = MetricDataset(video_path=args.video_path,
                          use_video=use_video, device=device)
        
    else:
        raise AssertionError("You should give either (audio, frame) or (video)")

    dataloader = DataLoader(dataset, batch_size=args.batch_size, shuffle=False,
                            num_workers=args.num_workers)

    # Model
    model = SyncNet().to(device)
    print('total trainable params {}'.format(sum(p.numel() for p in model.parameters() if p.requires_grad)))

    if checkpoint_path is not None:
        model = load_checkpoint(checkpoint_path, model)

    out = eval_model(dataloader, device, model.eval())
    pretty_print(args.audio_path, args.image_dir, args.video_path,
                 use_frame, use_video, **out)